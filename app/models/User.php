<?php


class User extends CActiveRecord
{
    const STATUS_PENDING = 0;
    const STATUS_CONFIRMED = 1;


    public function tableName()
    {
        return 'user';
    }

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function rules()
    {
        return array(
            array('email', 'required'),
            array('email', 'email'),
            array('email', 'unique'),
            array('password, activation_code, status', 'safe')
        );
    }

    public function sendActivationLink()
    {
        $mail = Yii::app()->email;
        $mail->from = "kr.vital@gmail.com";
        $mail->to = $this->email;
        $mail->subject = 'Activation link';
        $mail->message = 'Welcome! \n Here is activation link <a href="' .
            Yii::app()->createAbsoluteUrl(
                "/site/emailConfirmation",
                array("user_id" => $this->id, "code" => $this->activation_code)
            ) . '">click here</a>';
        return $mail->send();
    }

    public function sendPassword()
    {
        $mail = Yii::app()->email;
        $mail->to = $this->email;
        $mail->subject = 'Password';
        $mail->message = 'Here is your password:  ' . $this->password;
        return $mail->send();
    }

    public function isActive()
    {
        return (bool)$this->status;
    }

    public function codeValidity($code)
    {
        return $this->activation_code == $code;
    }

    protected function beforeSave()
    {
        if ($this->isNewRecord) {
            $this->setupActivationCode();
            $this->setupPassword();
            $this->status = self::STATUS_PENDING;
        }
        return parent::beforeSave();
    }

    private function setupActivationCode()
    {
        $this->activation_code = md5(time() . $this->email);
    }

    private function setupPassword()
    {
        $this->password = $this->generatePassword();
    }

    private function generatePassword()
    {
        $alpha = "abcdefghijklmnopqrstuvwxyz";
        $numeric = "0123456789";
        $pass_length = 8;
        return substr(str_shuffle($alpha . $numeric), 0, $pass_length);
    }

    public function regenerateSession()
    {
        if(!empty($this->session_id)) {
            Yii::app()->session->destroySession($this->session_id);
        }

        $this->session_id = Yii::app()->session->sessionID;
        $this->save();
    }

}