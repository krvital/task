<div class="form well span4 offset1">
    <?php
    $form = $this->beginWidget(
        'bootstrap.widgets.TbActiveForm',
        array(
             'type' => 'inline',
             'enableAjaxValidation' => true,
        )
    );
    ?>

    <fieldset>
        <?php $this->widget(
            'bootstrap.widgets.TbAlert',
            array(
                 'block'     => true,
                 'fade'      => true,
                 'closeText' => '&times;',
                 'alerts'    => array(
                     'success' => array(
                         'block'     => true,
                         'fade'      => true,
                         'closeText' => '&times;'
                     ),
                 ),
            )
        ); ?>
        <legend>Login</legend>
        <?php echo $form->errorSummary($model); ?>
        <?php echo $form->textFieldRow($model, 'email', array('placeholder' => 'Username', 'class' => 'span4', 'autofocus' => 'true'));?>
        <p></p>
        <?php echo $form->passwordFieldRow($model, 'password', array('class' => 'span4'));?>
        <p></p>
        <?php $this->widget(
            'bootstrap.widgets.TbButton',
            array(
                 'buttonType' => 'submit',
                 'label'       => 'Sign In',
                 'type'        => 'primary',
                 'block'       => true,
            )
        ); ?>
        <br>
        <p style="text-align: center"><a href="<?php echo Yii::app()->createUrl('/site/register')?>">Don't have account? Register now</a></p>
    </fieldset>
    <p></p>
    <hr>

    <?php $this->endWidget(); ?>
</div><!-- form -->