<div class="form well span4 offset1">
    <?php
    $form = $this->beginWidget(
        'bootstrap.widgets.TbActiveForm',
        array(
             'id'                   => 'registration-form',
             'type'                 => 'inline',
             'enableAjaxValidation' => true,
             'clientOptions'        => array(
                 'validateOnChange' => true,
                 'validateOnSubmit' => true
             )
        )
    );
    ?>
    <fieldset>
        <?php $this->widget(
            'bootstrap.widgets.TbAlert',
            array(
                 'block'     => true,
                 'fade'      => true,
                 'closeText' => '&times;',
                 'alerts'    => array(
                     'success' => array(
                         'block'     => true,
                         'fade'      => true,
                         'closeText' => '&times;'
                     ),
                 ),
            )
        ); ?>
        <legend>Registration</legend>
        <?php echo $form->errorSummary($model); ?>
        <?php echo $form->textFieldRow(
            $model,
            'email',
            array('placeholder' => 'Enter your email', 'class' => 'span4', 'autofocus' => 'true')
        );?>
        <p></p>

        <p></p>
        <?php $this->widget(
            'bootstrap.widgets.TbButton',
            array(
                 'buttonType' => 'submit',
                 'label'      => 'Register',
                 'type'       => 'primary',
                 'block'      => true,
            )
        ); ?>
    </fieldset>
    <p></p>
    <hr>
    <?php $this->endWidget(); ?>
</div><!-- form -->