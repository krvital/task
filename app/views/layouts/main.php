<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title><?php echo CHtml::encode($this->pageTitle); ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>

<body>

<div class="container-narrow">
    <div class="masthead">
        <?php $this->widget(
            'bootstrap.widgets.TbMenu',
            array(
                 'htmlOptions' => array(
                     'class' => 'nav nav-pills pull-right'
                 ),
                 'items'       => array(
                     array('label' => 'Home', 'url' => array('/site/index')),
                     array('label' => 'Login', 'url' => array('/site/login'), 'visible' => Yii::app()->user->isGuest),
                     array(
                         'label'   => 'Logout (' . Yii::app()->user->name . ')',
                         'url'     => array('/site/logout'),
                         'visible' => !Yii::app()->user->isGuest
                     ),
                     array('label'   => 'Register',
                           'url'     => array('/site/register'),
                           'visible' => Yii::app()->user->isGuest
                     ),
                 ),
            )
        ); ?>
        <h3 class="muted"><?php echo CHtml::encode(Yii::app()->name); ?></h3>
    </div>
    <hr>
    <?php echo $content; ?>
    <hr>
    <div class="footer">
        <p>&copy; Company 2013</p>
    </div>

</div>
<!-- /container -->

</body>
</html>
